# SFDX CLI

Salesforce DX cli Docker image

Lightweight Docker image using node alpine.

Includes:
- SalesforceDX CLI from NPM
- Salesforce CLI Scanner Plug-in
- Java
- jq for shell JSON parsing
- gettext for text file processing
- ca-certificates, openssl for test result and artifact storage on CircleCI
- openssh for CircleCI SSH access

Image on docker hub:
https://hub.docker.com/r/ibvcloud/sfdx-cli

## How to build docker container

Run:
```shell
> docker build -t ibvcloud/sfdx-cli:latest .
> docker build -t ibvcloud/sfdx-cli:7.144.2 .

> docker push ibvcloud/sfdx-cli:latest
> docker push ibvcloud/sfdx-cli:7.144.2
```

