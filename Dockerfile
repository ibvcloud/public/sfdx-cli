#FROM node:alpine
FROM balenalib/amd64-alpine-node:latest

ENV VERSION=7.144.2
#ENV JAVA_ALPINE_VERSION 8.302.08-r2
#ENV JAVA_HOME /usr/lib/jvm/java-1.8-openjdk
#ENV PATH $PATH:/usr/lib/jvm/java-1.8-openjdk/jre/bin:/usr/lib/jvm/java-1.8-openjdk/bin

RUN apk update
RUN apk add --update --no-cache git openssh ca-certificates openssl jq gettext xmlstarlet curl

# Install Java
#RUN apk add --no-cache openjdk8=${JAVA_ALPINE_VERSION}
#RUN apk add --no-cache supervisor=4.2.2-r2

RUN npm install sfdx-cli@${VERSION} --global

RUN sfdx --version
RUN sfdx plugins --core
